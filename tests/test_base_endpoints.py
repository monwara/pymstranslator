from unittest import TestCase

from mstranslator.endpoints.base import Endpoint


class BaseEndpoint(TestCase):
    def setUp(self):
        self.t = Endpoint()

    def test_get_url(self):
        self.t.endpoint = 'Foo'
        self.assertEqual(
            self.t.get_endpoint_url(),
            'http://api.microsofttranslator.com/V2/HTTP.svc/Foo'
        )

    def test_get_quoted_params(self):
        self.t.params = {'foo': 'bar', 'bar': 1}
        self.assertEqual(
            self.t.get_quoted_params(),
            'foo=bar&bar=1'
        )

    def test_get_full_url(self):
        self.t.endpoint = 'Translate'
        self.t.params = {'text': 'foo'}
        self.assertEqual(
            self.t.get_request_url(),
            'http://api.microsofttranslator.com/V2/HTTP.svc/Translate?text=foo'
        )


    def test_camelize(self):
        self.assertEqual(
            self.t.camelize_param('foo_bar'),
            'fooBar'
        )

        self.assertEqual(
            self.t.camelize_param('foo_bar_fam'),
            'fooBarFam'
        )

    def test_convert_kwargs(self):
        kw = dict(foo='bar', tfrom='test', fmt='bonus', fam_dam=1)
        self.t.convert_kwargs(kw),
        self.assertEqual(self.t.params['foo'], 'bar')
        self.assertEqual(self.t.params['from'], 'test')
        self.assertEqual(self.t.params['format'], 'bonus')
        self.assertEqual(self.t.params['famDam'], 1)

