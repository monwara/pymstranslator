from unittest import TestCase
from os import path

from mstranslator import MSTranslatorAccessKey, MSTranslator

def load_sensitive_data(filename):
    f = open(
        path.join(path.dirname(path.realpath(__file__)), '%s.txt' % filename)
    )

    data = f.read().strip()
    f.close()
    return data

client_id = load_sensitive_data('client_id')
client_secret = load_sensitive_data('client_secret')


class TokenIntegrationTestCase(TestCase):
    def test_we_can_obtain_non_null_token(self):
        key = MSTranslatorAccessKey(client_id, client_secret)
        self.assertTrue(len(key.get_key()) > 0)


class ClientUnitTestCase(TestCase):
    def setUp(self):
        api_key = MSTranslatorAccessKey(client_id, client_secret)
        self.t = MSTranslator(api_key)

    def test_get_auth_string(self):
        self.assertTrue('Bearer' in self.t.get_authorization_string())

    def test_get_auth_header(self):
        self.assertTrue(
            'Bearer ' in self.t.get_authorization_header()['Authorization']
        )


class ClientTranslateTest(TestCase):
    def setUp(self):
        api_key = MSTranslatorAccessKey(client_id, client_secret)
        self.t = MSTranslator(api_key)

    def test_translate_basically_works(self):
        text = 'Si vous plais'
        translation = "If you'd like"

        self.assertEqual(
            self.t.translate(text=text, tfrom='fr', to='en'),
            translation
        )
